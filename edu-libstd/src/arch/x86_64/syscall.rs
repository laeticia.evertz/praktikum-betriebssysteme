//! Wrappers for x86_64's `syscall` instruction.
//!
//! In eduOS, `a0` is the system call number. You can provide up to six
//! additional arguments.
//!
//! # Safety
//!
//! Users need to ensure that the arguments make for a well-formed system call
//! and interpret the return value according to the respective system call.

/// # Safety
///
/// See the module description.
pub unsafe fn syscall0(a0: usize) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, out("rcx") _, out("r11") _);
	ret
}

/// # Safety
///
/// See the module description.
pub unsafe fn syscall1(a0: usize, a1: usize) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, in("rdi") a1, out("rcx") _, out("r11") _);
	ret
}

/// # Safety
///
/// See the module description.
pub unsafe fn syscall2(a0: usize, a1: usize, a2: usize) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, in("rdi") a1, in("rsi") a2, out("rcx") _, out("r11") _);
	ret
}

/// # Safety
///
/// See the module description.
pub unsafe fn syscall3(a0: usize, a1: usize, a2: usize, a3: usize) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, in("rdi") a1, in("rsi") a2, in("rdx") a3, out("rcx") _, out("r11") _);
	ret
}

/// # Safety
///
/// See the module description.
pub unsafe fn syscall4(a0: usize, a1: usize, a2: usize, a3: usize, a4: usize) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, in("rdi") a1, in("rsi") a2, in("rdx") a3, in("r10") a4, out("rcx") _, out("r11") _);
	ret
}

/// # Safety
///
/// See the module description.
pub unsafe fn syscall5(a0: usize, a1: usize, a2: usize, a3: usize, a4: usize, a5: usize) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, in("rdi") a1, in("rsi") a2, in("rdx") a3, in("r10") a4, in("r8") a5, out("rcx") _, out("r11") _);
	ret
}

/// # Safety
///
/// See the module description.
pub unsafe fn syscall6(
	a0: usize,
	a1: usize,
	a2: usize,
	a3: usize,
	a4: usize,
	a5: usize,
	a6: usize,
) -> usize {
	let ret: usize;
	asm!("syscall", inout("rax") a0 => ret, in("rdi") a1, in("rsi") a2, in("rdx") a3, in("r10") a4, in("r8") a5, in("r9") a6, out("rcx") _, out("r11") _);
	ret
}
