// Copyright (c) 2017-2020 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use crate::arch::stack::BOOT_STACK;
use crate::consts::STACK_SIZE;
use crate::edu_kernel_init;

#[no_mangle]
#[naked]
pub extern "C" fn _start() -> ! {
	unsafe {
		asm!(
			// Initialize rsp with BOOT_STACK.top()
			// Get (bottom) address of BOOT_STACK via RIP-relative addressing from GOT
			// See https://www.mindfruit.co.uk/2012/06/relocations-relocations.html#atgotpcrel
			"mov rsp, qword ptr [rip + {boot_stack}@GOTPCREL]",
			// Add offset to get the top of BOOT_STACK
			"add rsp, {top_offset}",

			// jump to start
			"call {start}",

			boot_stack = sym BOOT_STACK,
			top_offset = const STACK_SIZE - 16,
			start = sym start,
			options(noreturn)
		)
	}
}

#[linkage = "weak"]
#[no_mangle]
extern "C" fn start() -> ! {
	unsafe { edu_kernel_init() }

	#[cfg(not(test))]
	{
		use crate::scheduler::{self, TaskManager};
		use core::ptr;

		// If we don't use a symbol that is only available in libstd or the integration
		// test, weakly linked symbols are not overwritten.
		extern "C" {
			static FORCE_LINK: u8;
		}
		unsafe { ptr::read_volatile(&FORCE_LINK) };

		#[linkage = "weak"]
		#[export_name = "_init"]
		extern "C" fn integration_test_init() {}

		integration_test_init();

		extern "C" fn libstd_runner(_: *mut ()) -> ! {
			#[linkage = "weak"]
			#[no_mangle]
			pub extern "C" fn libstd_init() -> ! {
				unsafe { asm!("syscall", inout("rax") 0 => _, out("rcx") _, out("r11") _) }
				unreachable!();
			}
			libstd_init()
		}

		scheduler::get()
			.lock(|task_manager| task_manager.spawn_user(libstd_runner, ptr::null_mut()));
		TaskManager::exit(scheduler::get(), Ok(()))
	}

	#[cfg(test)]
	{
		crate::test_main();
		crate::arch::processor::exit(0)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	/// Verify boot stack calculation
	fn test_boot_stack() {
		unsafe {
			// Theory
			assert_eq!(
				BOOT_STACK.top(),
				&BOOT_STACK as *const _ as usize + STACK_SIZE - 16,
			);

			// Practice
			let rsp: usize;
			asm!(
				"mov {rsp}, qword ptr [rip + {boot_stack}@GOTPCREL]",
				"add {rsp}, {top_offset}",

				rsp = out(reg) rsp,
				boot_stack = sym BOOT_STACK,
				top_offset = const STACK_SIZE - 16,
				options(nostack)
			);

			assert_eq!(BOOT_STACK.top(), rsp);
		}
	}
}
