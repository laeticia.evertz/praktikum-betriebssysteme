//! This test runs the kernel boot code, prints one line and then shuts itself
//! down.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

#[macro_use]
extern crate edu_kernel;

#[no_mangle]
static FORCE_LINK: u8 = 0;

/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	println!("Kernel booted.");
}
